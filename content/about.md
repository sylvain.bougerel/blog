+++
title = 'About Me'
date = 2024-02-23T09:51:23+08:00
draft = false
menu = "main"
weight = 50
+++
# Hi, I'm Sylvain Bougerel
I'm a software engineer with over a decade of experience working on mission critical & high-availability systems at [Grab](https://www.grab.com/) and [Thales](https://www.thalesgroup.com/en/global/group), in cloud or on-premise.
Over my career, I grew to enjoy focusing on engineering efficiency topics; devops, continuous integration and delivery, infrastructure maintenance, site reliability engineering.

I usually spend my free time working on the tools I love and use daily or on some projects of the moment, that I usually share on [Github](https://github.com/sbougerel).
I'm a long time Linux and Emacs user, normally running Arch but increasingly getting into Nix.
I'm currently interested in improving my note taking workflows with `org-mode` and `org-roam`.

In recent years, I became interested in topics related to the environment.
As I became more concerned with the climate crisis, the collapse of our biodiversity, the production of decarbonated energy, I started to research the contributions I can make in this field.

I'm now looking to marry my experience in software engineering with my passion for the environment. If you're working in green tech or climate tech, I would love to work with you or hear about what you do. You can find me on [LinkedIn](https://www.linkedin.com/in/sylvain-bougerel/) or drop me [an email](mailto:sylvain.bougerel@gmail.com).

## Projects

### [logseq-org-roam](https://github.com/sbougerel/logseq-org-roam)
The last project I worked on. I currently prefer [Logseq](https://logseq.com/) for `org` note taking on the go. I wrote this project to make `org-roam` and Logseq more interoperable, so I can continue to use both together at my desk and on the go.

### [autosync-magit](https://github.com/sbougerel/autosync-magit)
A simple package to automate synchronisation with a Git remote and other tasks, when using Emacs. This tool is designed for personal use, in places where you'd prefer to use Git over something such as Syncthing; e.g. to synchronise personal notes.

### [Spatial C++ Library](https://github.com/sbougerel/spatial)
A generic C++ library that I wrote with the intention of providing re-balancing K-d trees structures in a familiar STL-like API, supporting common spatial operations. Re-balancing K-d trees are useful in scenarios where a large number of objects are constantly updated.

### Contributor to [Libkdtree++](https://github.com/nvmd/libkdtree)
One of the main contributors to libkdtree++, notably improved the performance of the nearest-neighbour search algorithm by 2 orders of magnitude.
