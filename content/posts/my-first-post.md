+++
title = "My first post from org-mode"
author = ["Sylvain Bougerel"]
date = 2024-02-20T15:14:00+08:00
tags = ["emacs", "org"]
categories = ["blog"]
draft = false
+++

This post was exported from Emacs `org-mode` (with `org-roam`) using `ox-hugo`.

Yes, that's all there is to that post. But I'm gonna leave it there for keepsake.
