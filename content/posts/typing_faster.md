+++
title = "Typing faster"
author = ["Sylvain Bougerel"]
date = 2024-08-24T16:26:00+08:00
tags = ["blog"]
categories = ["blog"]
draft = false
+++

I've always been rather ashamed of my shabby typing speed. When I code, I sometimes spend as much time correcting as writing. It's painful to watch, it's such a dent on productivity.

Last March, I started to use [MonkeyType](https://monkeytype.com) and since June, I also added [Keybr](https://keybr.com) to my routine. It's been 6 months, and I've made noticeable progress. Not as fast as I hoped to be frank, but I'm satisfied with the results so far. This is how I do it.

{{< figure src="/ox-hugo/Screenshot from 2024-08-24 11-01-21.png" >}}


## MonkeyType {#monkeytype}

I use MonkeyType to train muscle memory. MonkeyType generates a random sequence of words from a dictionary and allows you to practice from your mistakes. MonkeyType is highly configurable. I generally do the following:

-   Use large dictionaries (English 10K): get my finger randomized on typing patterns. I don't want my fingers to focus on learning a few thousand words. Often I type words that do not exist in any dictionary since I tend to abbreviate or use mathematical symbols. Thus I prefer to train my fingers to be accurate on a wide-range of key transitions.
-   Add punctuation: since I write code
-   Do time-based exercises (60 or 120 mins)
-   Practice after each exercise to learn the patterns that I missed. I repeat practice until I hit my accuracy target (currently 97%). When I do, I start another time-based exercise. Rinse and repeat.
-   Fix _all_ mistakes, no exceptions: making mistakes is part of the typing experience, if you don't have to fix your mistakes you're not really evaluating your speed. My tests always finish "clean".

Some additional remarks when using MonkeyType:

-   I used "Expert" mode for a while (slaps you if you leave a mistake in a word before you terminate that word.) This was a huge mistake, I ended up getting scared of hitting the space bar on my keyboard. It was not for me, I reverted it.
-   I used to auto-cancel the exercise if my accuracy drops below a certain threshold. This resulted in incomplete tests and artificially inflating my stats. Don't use it if you really want to train.
-   I also typed with numbers originally. It was too big of a hit on my typing speed. I will re-introduce numbers at some point, but this taught me that I need to take it step-by-step.

I really love the flexibility and the statistics MonkeyType provides.


## Keybr {#keybr}

I started to use Keybr much later. I regret it. While MonkeyType trains muscle memory, Keybr trains your laziest fingers. For me, it's my left hand's Q, W and Z. I'm awful on my left hand, and it ranges from 40WPM to 80WPM between my right and left hand.

When using Keybr, I set a typing speed target that I can comfortably reach to pass all the letters. I then increase by 1WPM, pass all the letters and repeat. Similarly to MonkeyType, Keybr is highly configurable. I settled on the following:

-   Disable soft correction, force correction of every single mistakes: again, I'm not trying to learn type-racing, I want to learn accuracy and fixing mistakes is part of the experience.
-   15 minutes objective per session of Keybr: I find the default of 30 minutes to be too long, the truth is that a 30 minutes session is much more in real time, because of the downtime between exercises.


## The routine {#the-routine}

I start my routine with Keybr, then I move on to MonkeyType. I generally do 15 minutes of Keybr, then 15 minutes of MonkeyType. This way I practice both my laziest fingers and my muscle memory.

Ideally I'd train every day, but due to constraints, it ends up being 3 to 4 times a week. I started to work early May, and you can clearly see the impact on the statistics.


## Whatever you do, accuracy is king {#whatever-you-do-accuracy-is-king}

I don't think I actually improved much in typing speed, to be precise. But I make a lot less mistakes. When I started on my journey, I was doing 80% accuracy. I would watch videos of folks saying: "You should aim for 95% accuracy at least." And I was shocked: how could you do 95% all the time?!

6 months later, I don't like when I do less than 97% accuracy. And I reach it regularly. Sometimes, I even go through a stretch of 45 seconds where I don't make a single mistake. This yields huge speed gains. And with the addition of Keybr, I actually think I did make some improvements in speed (even discounting mistakes).

{{< figure src="/ox-hugo/_20240824_114651screenshot.png" >}}

I removed practice from the statistics above so it only shows what I consider to be "true" tests: randomized input seen for the first time and typed in one take.

I started my journey at around ~40WPM with &lt;90% accuracy. Now, I am starting to break into the 60-70WPM range with a 95% accuracy. So it took me 6 months to gain around 20WPM.

My target is to reach 80WPM, ideally with numbers. Based on current trend, I probably have another year or so to train, so I'll report back in a year from now.
